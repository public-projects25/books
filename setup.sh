#!/bin/bash
docker-compose up -d --build
docker-compose exec web alembic upgrade head
docker-compose exec web pytest .