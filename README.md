
# Setup
### 1. Replace Environmental Variables
In the root directory of the project is the `.env` file. 
The secret variables have been committed with the string
**"secret-to-be-updated"**. Please create your own 
passwords and keys before starting the application. 
It works perfectly without these adjustments, but is 
unrealistic and makes the application much less secure.

## Automated Setup and Testing
### 2. Using Shell Script (tested on Linux/WSL)
In a terminal make the shell script `setup.sh` executable (e.g. `chmod +x ./setup.sh`) and 
then run `./setup.sh`. 

Now continue with step 5 (accessing via the web).

## Manual Setup and Test

### 2. Build and Run Image

In a terminal run the following command

`docker-compose up -d --build`

### 3. Upgrade the DB
Run the following command to upgrade the database

   `docker-compose exec web alembic upgrade head`


### 4. Run Tests

To check everything is up and running as expected, run the tests
with the command

`docker-compose exec web pytest .`


### 5. Access the FastAPI Docs
- `http://localhost:9002/docs#` This will allow you to see the swagger docs as well as 
   try out the various apis through the browser. 
- To get started:
1. To create a new user, use the `/register` endpoint
2. Now either login using the `/login` endpoint or 
    the `Authorize` button at the top left-hand corner of the 
    webpage. You will need your email address (`username` field) and password to login.
3. Play to your hearts content :)

   
    Note: the `get` and `search` endpoints do not require authorization
    but all the others do (as indicated by the lock icon in the webpage)

### 6. Shutdown application

- use `docker-compose down` to shutdown the application and 
 `docker-compose down --volumes` to remove the volume mount
  (loses all the data in the database and you'll need to run the
  alembic upgrades from scratch).

# Developer Contributions

### Environmental Setup

#### Dependencies
- Docker Desktop
- Python3 (version in Pipfile)

#### Local Setup

1. `pip install pipenv`
2. Create virtual environment `pipenv shell`
3. Install dependencies `pipenv install -d`
4. Follow setup steps to get the docker image built and working.
pytests are run on the docker image as it has access to the database.

To add new packages add them to the Pipfile and then rerun step 3.

When running mypy it may give stubs errors. To resolve this, 
run `mypy --install-types` in your virtual environment.

##### Tests
- To get coverage run:
`docker-compose exec web coverage run -m pytest .`
- Then use the following to see the report
`docker-compose exec web coverage report -m`

#### Changes to DB Schema
1. list all the running containers 

   `docker ps`
2. exec into the container running the **book-api_web** image
`docker exec -it <app-container-id> bash`
3. Run the following command to create a database revision
file with alembic

    `alembic revision --autogenerate -m "<upgrade message>"`
3. Run the following command to upgrade the database

   `alembic upgrade head`
4. Exit the pod `exit`

# Next Steps
- Run a security scan on the docker image to find any vulnerabilities
- Check for any sql injection threats (don't think it's a problem, but need to confirm) 
- Check the scaling of apis, particularly for the **get** requests. Introduce limits and pagination.
- Add an external database with persistent storage and redundancy (maybe even different AZs for DR events)
- Schedule a DR test
- \figure out the deployment strategy (i.e. AKS, EKS, self-hosted, etc.). This
will impact the networking and image repo among other infrastructure
considerations.
- Consider the networking best practices - maybe we need some DMZs/VPCs
- Build a React frontend if required and potentially implement  solution using nginx
- Add a 'super user'  role and give it elevated abilities and rights
- Implement a refresh token endpoint
- Implement a more secure JWT encryption algorithm (rs256 maybe) 
- Define the allowed origins for the frontend to ensure greater security
  (done with the CORS middleware layer in FastAPI)
- Add user management endpoints if required.
- Extend search to not be exact, but use like

# Engaging with Team
- Is a frontend required and if so what should it look like?
- What size of user base are we expecting? What is the roll-out strategy?
 Generally, if the roll-out is gradual we do not need to harden the
 application as much at the outset.
- Do we need to worry about different countries laws and data privacy?
- What kind of redundancy is needed? This could be determined by load 
(increase the number of workers) or by availability 
(more AZs or geo-spatial redundancy)
- Would it be possible to use cloud functions/lambdas? 
These are templated and offer a lot of scaling out of the box. 
The number of get requests may turn out to be a bottleneck.
- The APIS are JSON as a start for simplicity and speed 
(since the underlying data model is independent of responses). 
An XML structure is of course doable and would be added should
this prove necessary.
- Perhaps we could consider using an authentication tool 
(such as Keycloak, AWS Cognito, etc.). 
They are considered more secure than managing and maintaining 
our own algorithms. 
- Will we require analytics on the underlying data? If so, it
can prove faster to process this as events are generated rather
than aggregated afterwards. We can think about how best to do
this within the current framework.
- Would we like a wild-card search? We could implement this