from datetime import datetime
from typing import Optional

from pydantic import BaseModel, constr


class UserBaseSchema(BaseModel):
    name: str
    email: str
    photo: str
    pseudonym: Optional[str] = None

    class Config:
        orm_mode = True


class CreateUserSchema(UserBaseSchema):
    password: constr(min_length=8)
    passwordConfirm: str
    role: str = "user"
    verified: bool = False


class LoginUserSchema(BaseModel):
    email: str
    password: constr(min_length=8)


class UserResponse(UserBaseSchema):
    id: int
    created_at: datetime
    updated_at: datetime


class BookBaseSchema(BaseModel):
    title: str
    description: str

    class Config:
        orm_mode = True


class CreateBookSchema(BookBaseSchema):
    published: Optional[bool] = False
    user_id: Optional[int] = 0


class UpdateBookSchema(BookBaseSchema):
    published: bool = False


class BookResponse(BookBaseSchema):
    id: int
    user_id: int
    pseudonym: Optional[str]
    created_at: datetime
    updated_at: datetime
