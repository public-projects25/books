import pytest
from app.database import database
from app.config import DATABASE_URL_STR


@pytest.fixture()
async def init_db():
    conn = await database.engine.set_bind(DATABASE_URL_STR)
    yield conn
    await conn.close()
