import pytest

from app.database.database import get_db
from . import init_db
from ..database.models import User


class TestAPI:
    @staticmethod
    async def remove_user(user_to_create):
        db_gen = get_db()
        db = next(db_gen)
        test_user = db.query(User).filter(User.email == user_to_create.email).first()
        db.query(User).filter(User.email == test_user.email).delete()
        db.commit()

    @pytest.mark.asyncio
    async def test_user_login_with_non_existing_username(self, test_app):
        fake_user = {"username": "non-existing-username", "password": "fake-password"}
        response = test_app.post(
            "api/auth/login",
            data=fake_user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )
        assert response.status_code == 400
        data = response.json()
        expected_response = {"detail": "Incorrect Email or Password"}
        assert data == expected_response

    @pytest.mark.asyncio
    async def test_user_login_with_existing_username(self, test_app, user_to_create):
        response = test_app.post("api/auth/register", json=user_to_create.dict())
        assert response.status_code == 201
        data = response.json()
        assert data["email"] == user_to_create.email

        user = {"username": user_to_create.email, "password": user_to_create.password}
        response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        await self.remove_user(user_to_create=user_to_create)
        assert response.status_code == 200
        data = response.json()
        expected_response_keys = ["access_token", "token_type"]
        assert list(data.keys()) == expected_response_keys
