from typing import Dict

import pytest
from requests import Response

from app.database.database import get_db
from . import init_db
from ..database.models import User
from ..database.schemas import CreateUserSchema


class TestAPI:
    @staticmethod
    async def remove_user(user_to_create):
        db_gen = get_db()
        db = next(db_gen)
        test_user = db.query(User).filter(User.email == user_to_create.email).first()
        db.query(User).filter(User.email == test_user.email).delete()
        db.commit()

    @staticmethod
    def extract_user_registered_successfully(
        response: Response, user: CreateUserSchema
    ):
        assert response.status_code == 201
        data = response.json()
        assert data["email"] == user.email
        return {"username": user.email, "password": user.password}

    @staticmethod
    def extract_token_from_login(response: Response):
        assert response.status_code == 200
        data = response.json()
        expected_response_keys = ["access_token", "token_type"]
        assert list(data.keys()) == expected_response_keys
        return data["access_token"]

    @staticmethod
    def assert_authorization_failed(
        response: Response, expected_dict: Dict, code: int = 401
    ):
        assert response.status_code == code
        data = response.json()
        assert data is not None
        assert data == expected_dict

    @pytest.mark.asyncio
    async def test_create_user(self, test_app, user_to_create):
        response = test_app.post("api/auth/register", json=user_to_create.dict())
        assert response.status_code == 201
        data = response.json()
        assert data["email"] == user_to_create.email
        await self.remove_user(user_to_create=user_to_create)

    @pytest.mark.asyncio
    async def test_create_user_already_exists(self, test_app, user_to_create):
        response = test_app.post("api/auth/register", json=user_to_create.dict())
        assert response.status_code == 201
        data = response.json()
        assert data["email"] == user_to_create.email

        response = test_app.post("api/auth/register", json=user_to_create.dict())
        assert response.status_code == 409
        assert response.reason == "Conflict"
        await self.remove_user(user_to_create=user_to_create)

    @pytest.mark.asyncio
    async def test_create_user_password_mismatch(self, test_app, user_to_create):
        user_to_create.passwordConfirm = "aaaa"
        response = test_app.post("api/auth/register", json=user_to_create.dict())

        self.assert_authorization_failed(
            response, {"detail": "Passwords do not match"}, 400
        )

    @pytest.mark.asyncio
    async def test_create_user_password_incorrect(self, test_app, user_to_create):

        response = test_app.post("api/auth/register", json=user_to_create.dict())

        user = self.extract_user_registered_successfully(response, user_to_create)

        user["password"] = "aaaa"

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )
        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(
            response, {"detail": "Incorrect Email or Password"}, 400
        )

    @pytest.mark.asyncio
    async def test_user_can_use_me_endpoint(self, test_app, user_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.get(
            "api/users/me",
            headers={"Authorization": f"Bearer {access_token}"},
        )

        await self.remove_user(user_to_create=user_to_create)

        assert response.status_code == 200
        data = response.json()
        assert data is not None
        assert isinstance(data, dict)
        assert data["name"] == user_to_create.name
        assert data["email"] == user_to_create.email
        assert data["photo"] == user_to_create.photo
        assert data["pseudonym"] == user_to_create.pseudonym

    @pytest.mark.asyncio
    async def test_user_me_endpoint_unauthorized(self, test_app, user_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        self.extract_user_registered_successfully(response, user_to_create)
        response: Response = test_app.get(
            "api/users/me",
        )

        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(response, {"detail": "Not authenticated"}, 401)

    @pytest.mark.asyncio
    async def test_user_me_endpoint_invalid_credentials(self, test_app, user_to_create):
        access_token: str = "jkgkjdgdkgkfdgjfd"
        response: Response = test_app.get(
            "api/users/me",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assert_authorization_failed(
            response, {"detail": "Could not validate credentials"}, 401
        )
