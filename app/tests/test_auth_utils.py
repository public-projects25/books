from app.auth.utils import hash_password, verify_password


class TestAuthUtils:
    def test_hash_unhash_password(self):
        pwd = "crickets"
        hashed_password = hash_password(pwd)
        assert verify_password(pwd, hashed_password)
