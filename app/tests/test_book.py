from typing import Dict

import pytest
from requests import Response
from starlette.testclient import TestClient

from app.database.database import get_db
from app.database.models import User, Book
from app.main import app
from . import init_db
from ..database.schemas import CreateUserSchema, CreateBookSchema

client = TestClient(app)


class TestAPI:
    @staticmethod
    async def remove_user(user_to_create):
        db_gen = get_db()
        db = next(db_gen)
        test_user = db.query(User).filter(User.email == user_to_create.email).first()
        db.query(User).filter(User.email == test_user.email).delete()
        db.commit()

    @staticmethod
    async def remove_book(book_to_create):
        db_gen = get_db()
        db = next(db_gen)
        test_book = db.query(Book).filter(Book.title == book_to_create.title).first()
        db.query(Book).filter(Book.title == test_book.title).delete()
        db.commit()

    @staticmethod
    def extract_user_registered_successfully(
        response: Response, user: CreateUserSchema
    ):
        assert response.status_code == 201
        data = response.json()
        assert data["email"] == user.email
        return {"username": user.email, "password": user.password}

    @staticmethod
    def extract_token_from_login(response: Response):
        assert response.status_code == 200
        data = response.json()
        expected_response_keys = ["access_token", "token_type"]
        assert list(data.keys()) == expected_response_keys
        return data["access_token"]

    @staticmethod
    def extract_successfully_created_book(response: Response, book: CreateBookSchema):
        assert response.status_code == 201
        data = response.json()
        if isinstance(data, dict):
            assert data["title"] == book.title
            assert data["description"] == book.description
            return data
        elif isinstance(data, list):
            assert response.status_code == 201
            data = response.json()
            assert data[0]["title"] == book.title
            assert data[0]["description"] == book.description
        else:
            raise AssertionError

    @staticmethod
    def assert_null_response(response: Response, code: int = 201):
        assert response.status_code == code
        data = response.json()
        assert data is None or data == []

    @staticmethod
    def assert_authorization_failed(
        response: Response, expected_dict: Dict, code: int = 401
    ):
        assert response.status_code == code
        data = response.json()
        assert data is not None
        assert data == expected_dict

    @staticmethod
    def assert_book_in_response(response: Response, book: Dict):
        assert response.status_code == 200
        data = response.json()
        assert data is not None
        assert len(data) > 0
        if isinstance(data, dict):
            assert data["id"] == book["id"]
            assert data["user_id"] == book["user_id"]
        else:
            found_book = [x for x in data if x["id"] == book["id"]]
            assert found_book is not None and len(found_book) == 1
            assert found_book[0]["user_id"] == book["user_id"]

    @staticmethod
    def assert_response_contains_parameter(
        response: Response, field: str, parameter: str, code: int = 200
    ):
        assert response.status_code == code
        data = response.json()
        assert data is not None
        assert len(data) > 0
        if isinstance(data, dict):
            assert data[field] == parameter
        else:
            for item in data:
                assert item[field] == parameter

    @staticmethod
    def assert_book_updated(
        response: Response, expected_book: Dict, updated_title: str
    ):
        assert response.status_code == 201
        data = response.json()
        assert data is not None
        assert len(data) > 0

        assert isinstance(data, dict)
        assert data["id"] == expected_book["id"]
        assert data["user_id"] == expected_book["user_id"]
        assert data["title"] == updated_title
        assert data["title"] != expected_book["title"]

    @staticmethod
    def assert_books_match(response: Response, expected_book: Dict):
        assert response.status_code == 201
        data = response.json()
        assert data is not None
        assert len(data) > 0

        assert isinstance(data, dict)
        assert data == expected_book

    @staticmethod
    def assert_update_or_delete_not_possible(
        response: Response, book: Dict, book_2: Dict, user: CreateUserSchema
    ):
        assert response.status_code == 400
        data = response.json()
        assert data is not None
        assert len(data) > 0

        assert isinstance(data, dict)
        expected_response: Dict = {
            "detail": f'No book with id {book["id"]} was found for user {book_2["user_id"]}: {user.email}'
        }
        assert data == expected_response

    @pytest.mark.asyncio
    async def test_create_book(self, test_app, user_to_create, book_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)

        self.extract_successfully_created_book(response, book_to_create)

    @pytest.mark.asyncio
    async def test_create_book_published_not_set(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        book_to_create.published = None
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        book_to_create.published = False
        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)

        self.extract_successfully_created_book(response, book_to_create)

    @pytest.mark.asyncio
    async def test_create_book_unauthorized(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        self.extract_user_registered_successfully(response, user_to_create)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
        )

        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(response, {"detail": "Not authenticated"}, 401)

    @pytest.mark.asyncio
    async def test_create_book_invalid_credentials(
        self, test_app, user_to_create, book_to_create
    ):
        access_token: str = "jkgkjdgdkgkfdgjfd"
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assert_authorization_failed(
            response, {"detail": "Could not validate credentials"}, 401
        )

    @pytest.mark.asyncio
    async def test_create_book_missing_title(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)

        book_to_create.title = None
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(
            response,
            {
                "detail": [
                    {
                        "loc": ["body", "title"],
                        "msg": "none is not an allowed value",
                        "type": "type_error.none.not_allowed",
                    }
                ]
            },
            422,
        )

    @pytest.mark.asyncio
    async def test_create_book_missing_description(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)

        book_to_create.description = None
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(
            response,
            {
                "detail": [
                    {
                        "loc": ["body", "description"],
                        "msg": "none is not an allowed value",
                        "type": "type_error.none.not_allowed",
                    }
                ]
            },
            422,
        )

    @pytest.mark.asyncio
    async def test_book_read_no_books(self, test_app, user_to_create, book_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)

        response: Response = test_app.post(
            "api/books/read",
            headers={"Authorization": f"Bearer {access_token}"},
        )

        await self.remove_user(user_to_create=user_to_create)
        self.assert_null_response(response)

    @pytest.mark.asyncio
    async def test_read_created_book(self, test_app, user_to_create, book_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.post(
            "api/books/read", headers={"Authorization": f"Bearer {access_token}"}
        )

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)

        self.extract_successfully_created_book(response, book_to_create)

    @pytest.mark.asyncio
    async def test_read_book_unauthorized(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        self.extract_user_registered_successfully(response, user_to_create)
        response: Response = test_app.post(
            "api/books/read",
            json=book_to_create.dict(),
        )

        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(response, {"detail": "Not authenticated"}, 401)

    @pytest.mark.asyncio
    async def test_read_book_invalid_credentials(
        self, test_app, user_to_create, book_to_create
    ):
        access_token: str = "jkgkjdgdkgkfdgjfd"
        response: Response = test_app.post(
            "api/books/read",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assert_authorization_failed(
            response, {"detail": "Could not validate credentials"}, 401
        )

    @pytest.mark.asyncio
    async def test_get_all_books_no_book_exists(self, test_app):
        response: Response = test_app.get("api/books/")
        self.assert_null_response(response, 200)

    @pytest.mark.asyncio
    async def test_get_all_books(self, test_app, user_to_create, book_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        book: Dict = self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.get("api/books")

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)

        self.assert_book_in_response(response, book)

    @pytest.mark.asyncio
    async def test_get_book_by_id(self, test_app, user_to_create, book_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        book: Dict = self.extract_successfully_created_book(response, book_to_create)
        response: Response = test_app.get(f"api/books/{book['id']}/")

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)

        self.assert_book_in_response(response, book)

    @pytest.mark.asyncio
    async def test_get_book_by_id_does_not_exist(self, test_app):
        response: Response = test_app.get("api/books/1000/")
        self.assert_null_response(response, 200)

    @pytest.mark.asyncio
    async def test_search_by_pseudonym_exists(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.get(
            f"api/books/search_by_pseudonym/?pseudonym={user_to_create.pseudonym}"
        )

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)

        self.assert_response_contains_parameter(
            response, "pseudonym", user_to_create.pseudonym
        )

    @pytest.mark.asyncio
    async def test_search_by_pseudonym_multiple_books(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.get(
            f"api/books/search_by_pseudonym/?pseudonym={user_to_create.pseudonym}"
        )

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)

        self.assert_response_contains_parameter(
            response, "pseudonym", user_to_create.pseudonym
        )
        assert len(response.json()) > 1

    @pytest.mark.asyncio
    async def test_search_by_pseudonym_does_not_exist(self, test_app):
        pseudonym = "p"
        response: Response = test_app.get(
            f"api/books/search_by_pseudonym/?pseudonym={pseudonym}"
        )
        self.assert_null_response(response, 200)

    @pytest.mark.asyncio
    async def test_update_book(self, test_app, user_to_create, book_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        book: Dict = self.extract_successfully_created_book(response, book_to_create)

        updated_book = book_to_create.copy(deep=True)
        updated_title = "Updated Book Title"
        updated_book.title = updated_title

        response: Response = test_app.put(
            f"api/books/update/{book['id']}/",
            json=updated_book.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        await self.remove_book(book_to_create=updated_book)
        await self.remove_user(user_to_create=user_to_create)

        self.assert_book_updated(response, book, updated_title)

    @pytest.mark.asyncio
    async def test_update_book_fails_for_wrong_author(
        self, test_app, user_to_create, user_to_create_2, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        book: Dict = self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.post(
            "api/auth/register", json=user_to_create_2.dict()
        )
        user_2 = self.extract_user_registered_successfully(response, user_to_create_2)

        response: Response = test_app.post(
            "api/auth/login",
            data=user_2,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token_2: str = self.extract_token_from_login(response)

        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token_2}"},
        )

        book_2: Dict = self.extract_successfully_created_book(response, book_to_create)

        updated_book = book_to_create.copy(deep=True)
        updated_title = "Updated Book Title"
        updated_book.title = updated_title

        response: Response = test_app.put(
            f"api/books/update/{book['id']}/",
            json=updated_book.dict(),
            headers={"Authorization": f"Bearer {access_token_2}"},
        )

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)
        await self.remove_user(user_to_create=user_to_create_2)

        self.assert_update_or_delete_not_possible(
            response, book, book_2, user_to_create_2
        )

    @pytest.mark.asyncio
    async def test_update_book_unauthorized(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        self.extract_user_registered_successfully(response, user_to_create)
        response: Response = test_app.put(
            "api/books/update/1/",
            json=book_to_create.dict(),
        )
        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(response, {"detail": "Not authenticated"}, 401)

    @pytest.mark.asyncio
    async def test_update_book_invalid_credentials(
        self, test_app, user_to_create, book_to_create
    ):
        access_token: str = "jkgkjdgdkgkfdgjfd"
        response: Response = test_app.put(
            "api/books/update/1/",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assert_authorization_failed(
            response, {"detail": "Could not validate credentials"}, 401
        )

    @pytest.mark.asyncio
    async def test_delete_book(self, test_app, user_to_create, book_to_create):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        book: Dict = self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.delete(
            f"api/books/delete/{book['id']}/",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        await self.remove_user(user_to_create=user_to_create)

        self.assert_books_match(response, book)

        response: Response = test_app.get(f"api/books/{book['id']}/")
        self.assert_null_response(response, 200)

    @pytest.mark.asyncio
    async def test_delete_book_fails_for_wrong_author(
        self, test_app, user_to_create, user_to_create_2, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        user = self.extract_user_registered_successfully(response, user_to_create)

        response: Response = test_app.post(
            "api/auth/login",
            data=user,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token: str = self.extract_token_from_login(response)
        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )

        book: Dict = self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.post(
            "api/auth/register", json=user_to_create_2.dict()
        )
        user_2 = self.extract_user_registered_successfully(response, user_to_create_2)

        response: Response = test_app.post(
            "api/auth/login",
            data=user_2,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

        access_token_2: str = self.extract_token_from_login(response)

        response: Response = test_app.post(
            "api/books/create",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token_2}"},
        )

        book_2: Dict = self.extract_successfully_created_book(response, book_to_create)

        response: Response = test_app.delete(
            f"api/books/delete/{book['id']}/",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token_2}"},
        )
        self.assert_update_or_delete_not_possible(
            response, book, book_2, user_to_create_2
        )

        response: Response = test_app.get(f"api/books/{book['id']}/")

        await self.remove_book(book_to_create=book_to_create)
        await self.remove_user(user_to_create=user_to_create)
        await self.remove_user(user_to_create=user_to_create_2)

        self.assert_book_in_response(response, book)

    @pytest.mark.asyncio
    async def test_delete_book_unauthorized(
        self, test_app, user_to_create, book_to_create
    ):
        response: Response = test_app.post(
            "api/auth/register", json=user_to_create.dict()
        )
        self.extract_user_registered_successfully(response, user_to_create)
        response: Response = test_app.delete(
            "api/books/delete/1/",
            json=book_to_create.dict(),
        )

        await self.remove_user(user_to_create=user_to_create)

        self.assert_authorization_failed(response, {"detail": "Not authenticated"}, 401)

    @pytest.mark.asyncio
    async def test_delete_book_invalid_credentials(
        self, test_app, user_to_create, book_to_create
    ):
        access_token: str = "jkgkjdgdkgkfdgjfd"
        response: Response = test_app.delete(
            "api/books/delete/1/",
            json=book_to_create.dict(),
            headers={"Authorization": f"Bearer {access_token}"},
        )
        self.assert_authorization_failed(
            response, {"detail": "Could not validate credentials"}, 401
        )
