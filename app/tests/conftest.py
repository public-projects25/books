import pytest
from starlette.testclient import TestClient

from app.database import schemas
from app.main import app


@pytest.fixture(scope="module")
def test_app():
    client: TestClient = TestClient(app)
    yield client


@pytest.fixture()
def book_to_create():
    yield schemas.CreateBookSchema(
        title="Test Book",
        description="Test Book Description",
        published=False,
        user_id=1,
    )


@pytest.fixture()
def user_to_create():
    yield schemas.CreateUserSchema(
        name="test_client@example.com",
        email="test_client@example.com",
        photo="person.jpeg",
        pseudonym="test_client",
        password="testclientpassword",
        passwordConfirm="testclientpassword",
        role="user",
        verified=True,
    )


@pytest.fixture()
def user_to_create_2():
    yield schemas.CreateUserSchema(
        name="test_client2@example.com",
        email="test_client2@example.com",
        photo="person.jpeg",
        pseudonym="test_client2",
        password="testclientpassword2",
        passwordConfirm="testclientpassword2",
        role="user",
        verified=True,
    )
