from typing import List, Optional

from fastapi import APIRouter, Depends, HTTPException, Path, status
from sqlalchemy import and_
from sqlalchemy.exc import ArgumentError
from sqlalchemy.orm import Session

import app.database.models as models
import app.database.schemas as schemas

from app.auth.deps import get_current_user
from app.database.database import get_db

router = APIRouter()


@router.get("/", summary="Get all books", response_model=List[schemas.BookResponse])
def get_books(db: Session = Depends(get_db)):
    books = db.query(models.Book, models.User).join(models.User).all()
    books_return = []
    for book, user in books:
        book_dict = book.__dict__
        book_dict["pseudonym"] = str(user.pseudonym)
        books_return.append(book_dict)
    return books_return


@router.get(
    "/{book_id}",
    summary="Get book by id",
    response_model=Optional[schemas.BookResponse],
)
def get_book_by_id(db: Session = Depends(get_db), book_id: int = Path(..., gt=0)):
    row = (
        db.query(models.Book, models.User)
        .join(models.User)
        .filter(models.Book.id == book_id)
        .first()
    )
    if not row:
        return None
    book: models.Book = row[0]
    user: models.User = row[1]
    book_dict = book.__dict__
    book_dict["pseudonym"] = str(user.pseudonym)
    return book_dict


@router.get(
    "/search_by_pseudonym/",
    summary="Get books written by author pseudonym",
)
def search_for_books_by_pseudonym(
    db: Session = Depends(get_db), pseudonym: Optional[str] = None
):
    books = (
        db.query(models.Book, models.User)
        .join(models.User)
        .filter(models.User.pseudonym == pseudonym)
        .all()
    )
    if len(books) == 0:
        return None
    elif len(books) == 1:
        book: models.Book = books[0][0]
        user: models.User = books[0][1]
        book_dict = book.__dict__
        book_dict["pseudonym"] = str(user.pseudonym)
        return book_dict
    else:
        books_return = []
        for book, user in books:
            book_dict = book.__dict__
            book_dict["pseudonym"] = str(user.pseudonym)
            books_return.append(book_dict)
        return books_return


@router.post(
    "/create",
    summary="Create new book",
    status_code=status.HTTP_201_CREATED,
    response_model=schemas.BookResponse,
)
async def create_book(
    payload: schemas.CreateBookSchema,
    db: Session = Depends(get_db),
    user: models.User = Depends(get_current_user),
):
    user = db.query(models.User).filter(models.User.id == user.id).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User not found. Please register.",
        )
    # ensure no null values
    if payload.title is None or payload.description is None:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="title and description may not be empty",
        )

    if payload.published is None:
        payload.published = False

    payload.user_id = user.id

    new_book = models.Book(**payload.dict())
    db.add(new_book)
    db.commit()
    db.refresh(new_book)
    return new_book


@router.post(
    "/read",
    summary="Get books for this author",
    status_code=status.HTTP_201_CREATED,
    response_model=List[schemas.BookResponse],
)
async def read_user_books(
    db: Session = Depends(get_db), user: models.User = Depends(get_current_user)
):
    user = db.query(models.User).filter(models.User.id == user.id).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User not found. Please register.",
        )
    book = db.query(models.Book).filter(models.Book.user_id == user.id).all()
    return book


@router.put(
    "/update/{book_id}/",
    summary="Update a book belonging to this author",
    status_code=status.HTTP_201_CREATED,
    response_model=schemas.BookResponse,
)
async def update_user_book(
    payload: schemas.UpdateBookSchema,
    book_id: int = Path(..., gt=0),
    db: Session = Depends(get_db),
    user: models.User = Depends(get_current_user),
):
    user = db.query(models.User).filter(models.User.id == user.id).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User not found. Please register.",
        )
    book: models.Book = (
        db.query(models.Book)
        .filter(and_(models.Book.user_id == user.id, models.Book.id == book_id))
        .first()
    )
    if not book:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"No book with id {book_id} was found for user {user.id}: {user.name}",
        )

    if payload.title is not None:
        book.title = payload.title

    if payload.description is not None:
        book.description = payload.description

    if payload.published is not None:
        book.published = payload.published
    db.add(book)
    db.commit()
    db.refresh(book)
    return book


@router.delete(
    "/delete/{book_id}/",
    summary="Delete a book belonging to this author",
    status_code=status.HTTP_201_CREATED,
    response_model=schemas.BookResponse,
)
async def delete_user_book(
    book_id: int = Path(..., gt=0),
    db: Session = Depends(get_db),
    user: models.User = Depends(get_current_user),
):
    user = db.query(models.User).filter(models.User.id == user.id).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User not found. Please register.",
        )
    book: models.Book = (
        db.query(models.Book)
        .filter(and_(models.Book.user_id == user.id, models.Book.id == book_id))
        .first()
    )
    if not book:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"No book with id {book_id} was found for user {user.id}: {user.name}",
        )

    db.query(models.Book).filter(
        and_(models.Book.user_id == user.id, models.Book.id == book_id)
    ).delete()
    db.commit()
    return book
