from fastapi import APIRouter, Depends

from app.auth.deps import get_current_user
from app.database.database import get_db
from sqlalchemy.orm import Session

import app.database.models as models
import app.database.schemas as schemas


router = APIRouter()


@router.get("/me", response_model=schemas.UserResponse)
def get_me(
    db: Session = Depends(get_db), user: models.User = Depends(get_current_user)
):
    user = db.query(models.User).filter(models.User.id == user.id).first()
    return user
