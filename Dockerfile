# pull official base image
FROM python:3.10.6-alpine

# install pipenv to manage dependencies
RUN pip install pipenv

# set work directory
WORKDIR /usr/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# copy requirements file
COPY Pipfile Pipfile.lock ${PROJECT_DIR}/

# install dependencies
RUN set -eux \
    && apk add --no-cache --virtual .build-deps build-base \
        libressl-dev libffi-dev gcc musl-dev python3-dev \
        postgresql-dev bash \
    && pip install --upgrade pip setuptools wheel \
    && pipenv install -d --system --deploy \
    && rm -rf /root/.cache/pip

# copy project
COPY . /usr/app/